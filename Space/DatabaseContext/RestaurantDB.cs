namespace Space.Database
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RestaurantDB : DbContext
    {
        public RestaurantDB()
            : base("name=RestaurantDB")
        {
        }

        public virtual DbSet<AccountLogs> AccountLogs { get; set; }
        public virtual DbSet<Accounts> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
