﻿using Space.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Space.Helpers
{
    public class DataAccessHelper
    {
        #region Members
        private static RestaurantDB _context;

        #endregion
        #region Properties
        private static RestaurantDB Context => _context ?? (_context = CreateContext());
        #endregion
        #region Methods
        /// <summary>
        /// Begins and returns a new transaction. Be sure to commit/rollback/dispose this transaction
        /// or use it in an using-clause.
        /// </summary>
        /// <returns>A new transaction.</returns>
        public static DbTransaction BeginTransaction()
        {
            // an open connection is needed for a transaction
            if (Context.Database.Connection.State == ConnectionState.Broken || Context.Database.Connection.State == ConnectionState.Closed)
            {
                Context.Database.Connection.Open();
            }

            // begin and return new transaction
            return Context.Database.Connection.BeginTransaction();
        }
        public static RestaurantDB CreateContext() => new RestaurantDB();

        /// <summary>
        /// Disposes the current instance of database context.
        /// </summary>
        public static void DisposeContext()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }

        public static bool Initialize()
        {
            using (RestaurantDB context = CreateContext())
            {
                try
                {
                    context.Database.Initialize(true);
                    context.Database.Connection.Open();
                    _ = MessageBox.Show($"[Load] Database has been initialized");
                }
                catch (Exception ex)
                {
                    string e = Convert.ToString(ex);
                    _ = MessageBox.Show("[Error] The Database is not up to date.", e);
                    return false;
                }
                return true;
            }
        }
        #endregion

    }
}
