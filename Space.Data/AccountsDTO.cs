﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Space.Data
{
    [Serializable]
    public class AccountsDTO
    {
      
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Password { get; set; }

        public DateTime? Joindate { get; set; }

        public string Authority { get; set; }

        public bool? Status { get; set; }

    }
}
