﻿using Space.DAL.DAO;
using Space.DAL.Interface;
using System;

namespace Space.DAL
{
    public class DaoFactory
    {
        #region members
        private static IAccountsDAO _accountsDAO;
        #endregion

        #region Properties
        public static IAccountsDAO AccountsDAO => _accountsDAO ?? (_accountsDAO = new AccountDAO());
        #endregion
    }
}
